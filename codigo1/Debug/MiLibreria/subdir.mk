################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../MiLibreria/l6474.c \
../MiLibreria/stm32f4xx_nucleo.c \
../MiLibreria/x_nucleo_ihm01a1_stm32f4xx.c \
../MiLibreria/x_nucleo_ihmxx.c 

OBJS += \
./MiLibreria/l6474.o \
./MiLibreria/stm32f4xx_nucleo.o \
./MiLibreria/x_nucleo_ihm01a1_stm32f4xx.o \
./MiLibreria/x_nucleo_ihmxx.o 

C_DEPS += \
./MiLibreria/l6474.d \
./MiLibreria/stm32f4xx_nucleo.d \
./MiLibreria/x_nucleo_ihm01a1_stm32f4xx.d \
./MiLibreria/x_nucleo_ihmxx.d 


# Each subdirectory must supply rules for building sources it contributes
MiLibreria/%.o MiLibreria/%.su: ../MiLibreria/%.c MiLibreria/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F401xE -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

clean: clean-MiLibreria

clean-MiLibreria:
	-$(RM) ./MiLibreria/l6474.d ./MiLibreria/l6474.o ./MiLibreria/l6474.su ./MiLibreria/stm32f4xx_nucleo.d ./MiLibreria/stm32f4xx_nucleo.o ./MiLibreria/stm32f4xx_nucleo.su ./MiLibreria/x_nucleo_ihm01a1_stm32f4xx.d ./MiLibreria/x_nucleo_ihm01a1_stm32f4xx.o ./MiLibreria/x_nucleo_ihm01a1_stm32f4xx.su ./MiLibreria/x_nucleo_ihmxx.d ./MiLibreria/x_nucleo_ihmxx.o ./MiLibreria/x_nucleo_ihmxx.su

.PHONY: clean-MiLibreria

