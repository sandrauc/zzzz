/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "math.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
uint8_t UTCOffset = 1; //Offset al estar en una zona horaria distinta de la de greenwich, por convenio con positivas hacia el este, en españa es +1
float temperatura = 12; //Por defecto se asume una temperatura de 12ºC
float presion = 101325;  //Por defecto se asume una presion de 1atm = 101325 Pa

//Estructuras
struct tiempo{
	uint8_t dia;
	uint8_t mes;
	uint16_t anio;
	uint8_t hora;
	uint8_t minuto;
	uint8_t segundo;
};

struct localizacion{
	float latitud;
	float longitud;
	float altitud;
};

//Funciones
//es_bisiesto devuelve 1 si el anio lo es o 0 si no lo es
uint8_t es_bisiesto(uint16_t anio){
	if((fmod(anio,4) == 0 && fmod(anio,100) != 0) || fmod(anio, 400) == 0){
		return 1;
	}
	else{
		return 0;
	}
}

//fecha2doy devuelve el día dentro del anio de una fecha (1 de enero corresponde a 1 y 12 de diciembre a 365 o 366)
uint16_t fecha2doy(struct tiempo t){
	uint8_t bisiesto = es_bisiesto(t.anio);
	uint8_t offsetMes[] = {1,2,0,1,1,2,2,3,4,4,5,5};
	uint16_t dia1 = ((t.mes - 1) * 30) + offsetMes[t.mes - 1];
	uint16_t doy = dia1 + (t.dia - 1);
	if(t.mes > 2){
		doy += bisiesto;
	}
	return doy;
}

float Dec2Rad(float ang){
	return ang * M_PI/180.0;
}
float Rad2Dec(float ang){
	return ang * 180/M_PI;
}

float sign(float num){
	if(num > 0){
		return 1;
	}
	else if (num < 0){
		return -1;
	}
	else{
		return 0;
	}
}

struct ephemeris{
	float Azimut;
	float Elevacion;
	float Aparente_Elevacion;
	float horaSolar;
};

//hay que ver que devuelve jeje xd y que pasarle
struct ephemeris ephemeritas(struct tiempo t, struct localizacion local){
	uint16_t doy = fecha2doy(t);
	float horaDec = t.hora + t.minuto/60.0 + t.segundo/3600.0;
	float fechaUniv = doy + floor((horaDec + UTCOffset)/24.0);
	float horaUniv = fmod((horaDec + UTCOffset), 24);
	float anio = t.anio - 1900;
	float anio_ini = 365 * anio + floor((anio-1)/4.0) - 0.5;
	float Ezero = anio_ini + fechaUniv; //no se que significa Ezero
	float T = Ezero / 36525.0; //siglo Julian de 36525 días
	float GMST0 = 6 / 24.0 + 38 / 1440.0 + (45.836 + 8640184.542 * T + 0.0929 * pow(T,2)) / 86400.0; //día sidéreo en el meridiano de Greenwich hora 0
	GMST0 = 360 * (GMST0 - floor(GMST0));
	float GMSTi = fmod(GMST0 + 360*(1.0027379093 * horaUniv / 24.0),360); //día sidéreo a la hora que es
	float LAST = fmod((360 + GMSTi - local.longitud), 360); //día sidereo aparente teniendo en cuenta la localización

	if(LAST < 0){
		LAST = 360 - LAST;
	}

	float fechaEpoch = Ezero + horaUniv/24.0;
	float T1 = fechaEpoch / 36525.0; //siglo julian de la epochDate
	float oblicuidad = Dec2Rad(23.452294 - 0.0130125 * T1 - 0.00000164 * pow(T1,2) + 0.000000503 * pow(T1,3)); //Calculo oblicuidad de la eclíptica
	float perigeo = 281.22083 + 0.0000470684 * fechaEpoch + 0.000453 * pow(T1,2) + 0.000003 * pow(T1,3); //cálculo perigeo del sol
	float meanSolarAnomaly = fmod((358.47583 + 0.985600267 * fechaEpoch - 0.00015 * pow(T1,2) - 0.000003 * pow(T1,3)), 360); //ángulo solar anomaly

	if(meanSolarAnomaly < 0){
		meanSolarAnomaly = 360 - meanSolarAnomaly;
	}

	float excentricidad = 0.01675104 - 0.0000418 * T1 - 0.000000126 * pow(T1,2);
	float ExcentAnom = meanSolarAnomaly;
	float E = 0;

	while(fabs(ExcentAnom - E) > 0.0001){
		E = ExcentAnom;
		ExcentAnom = meanSolarAnomaly + Rad2Dec(excentricidad*sin(Dec2Rad(E)));
	}

	float anomVerdadera = 2 * fmod(Rad2Dec(atan2(pow(((1 + excentricidad) / (1 - excentricidad)), 0.5) * tan(Dec2Rad(ExcentAnom / 2)), 1)), 360) ;

	if(anomVerdadera < 0){ //corrección necesaria ya que fmod en c y en matlab no trabajan igual con numeros negativos
		anomVerdadera = 2*360 + anomVerdadera;
	}

	float Abber = 20/3600.0;
	float ExLon = fmod(perigeo + anomVerdadera, 360) - Abber;
	float ExLon_Rad = Dec2Rad(ExLon);
	float Dec_Rad = asin(sin(oblicuidad) * sin(ExLon_Rad));
	float Dec = Rad2Dec(Dec_Rad);
	float RtAscen = Rad2Dec(atan2(cos(oblicuidad) * sin(ExLon_Rad), cos(ExLon_Rad)));
	float angHora = LAST - RtAscen;
	float angHora_Rad = Dec2Rad(angHora);

	angHora = angHora - (360 * sign(angHora) * (abs(angHora) > 180));

	float Latitud_Rad = Dec2Rad(local.latitud);
	float Azimut_Sol = Rad2Dec(atan2(-1 * sin(angHora_Rad), cos(Latitud_Rad) * tan(Dec_Rad) - sin(Latitud_Rad) * cos(angHora_Rad)));
	Azimut_Sol = Azimut_Sol + (Azimut_Sol < 0) * 360; //Cambio de rango de [-180,180] a [0,360]
	Azimut_Sol = Azimut_Sol + 180; //Cambio ángulo 0 es el Sur en vez de el Norte
	if(Azimut_Sol >= 360){
		Azimut_Sol = Azimut_Sol - 360;
	}
	float Elevacion_Sol = Rad2Dec(asin(cos(Latitud_Rad) * cos(Dec_Rad) * cos(angHora_Rad) + sin(Latitud_Rad) * sin(Dec_Rad)));

	float horaSolar = (180 + angHora) / 15.0;

	//Calculo refraccion de la luz hasta que el centro del sol está 1º por debajo del horizonte
	float tangenteElevacion = tan(Dec2Rad(Elevacion_Sol));
	float refraccion;
	if(Elevacion_Sol > 5 && Elevacion_Sol <= 85){
		refraccion = 58.1 / tangenteElevacion - 0.07 / pow(tangenteElevacion, 3) + 0.000086 / pow(tangenteElevacion, 5);
	}
	else if(Elevacion_Sol > -0.575 && Elevacion_Sol <= 5){
		refraccion = Elevacion_Sol *(-518.2 + Elevacion_Sol * (103.4 + Elevacion_Sol * (-12.79 + Elevacion_Sol * 0.711))) + 1735;
	}
	else if(Elevacion_Sol > -1 && Elevacion_Sol <= -0.575){
		refraccion = -20.774 / tangenteElevacion;
	}
	else{
		refraccion = 0;
	}

	refraccion = refraccion * (283 / (273.0 + temperatura)) * presion / 101325 / 3600;

	//La elevacion aparente tiene en cuenta la refraccion
	float Elevacion_Aparente = Elevacion_Sol + refraccion;

	struct ephemeris datos;
	datos.Azimut = Azimut_Sol;
	datos.Elevacion = Elevacion_Sol;
	datos.Aparente_Elevacion = Elevacion_Aparente;
	datos.horaSolar = horaSolar;
	return datos; //los ángulos se devuelven en grados

}

struct posSol{
	float x;
	float y;
	float z;
};

//función cálculo coordenadas cartesianas del sol, covenio:
//eje x -> oeste
//eje y -> sur
//eje z -> zenit
//los ángulos hay que introducirlos en grados
struct posSol pos_Sol (float azimut, float elevacion){
	struct posSol pos;
	pos.x = cos(Dec2Rad(elevacion)) * sin(Dec2Rad(azimut));
	pos.y = cos(Dec2Rad(elevacion)) * cos(Dec2Rad(azimut));
	pos.z = sin(Dec2Rad(elevacion));
	return pos;
}

float angulo_track(struct posSol pos){
	float angulo = atan(pos.x / pos.z);
	return Rad2Dec(angulo);
}

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  /* USER CODE BEGIN 2 */
  struct tiempo t = {22,07,2022,9,21,50};
  uint8_t a = fecha2doy(t);
  struct localizacion local = {40.45,-3.73,0};
  struct ephemeris b = ephemeritas(t, local);

  struct posSol pos = pos_Sol(b.Azimut, b.Elevacion);

  float c = angulo_track(pos);

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE2);

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 16;
  RCC_OscInitStruct.PLL.PLLN = 336;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV4;
  RCC_OscInitStruct.PLL.PLLQ = 7;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : B1_Pin */
  GPIO_InitStruct.Pin = B1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(B1_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : USART_TX_Pin USART_RX_Pin */
  GPIO_InitStruct.Pin = USART_TX_Pin|USART_RX_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  GPIO_InitStruct.Alternate = GPIO_AF7_USART2;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : LD2_Pin */
  GPIO_InitStruct.Pin = LD2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(LD2_GPIO_Port, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
